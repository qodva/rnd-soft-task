app.controller("ItemsCtrl", ["Item", "action", "NgTableParams", function(Item, action, NgTableParams){
    var ctrl = this;
    ctrl.itemsByPage = 1;
    action("index", function(){
       ctrl.tableItems = new NgTableParams({}, {
           counts: [5, 10, 25],
           getData: function(params){
               var requestParams = params.url();
               requestParams["search"] = ctrl.searchVal;
               return Item.query(requestParams).$promise.then(function(response){
                        var content = response[0];
                        params.total(content.count);
                        return content.data;
                   }
               )
           }
       });
    });
    action('show', function (params){
        ctrl.item = Item.get({id: params.id});
    });

    action('new', function(){
        ctrl.item = Item.new();
        ctrl.save = Item.create;
    });

    action('edit', function (params){
        ctrl.item = Item.edit({id: params.id});
        ctrl.save = Item.update;
    });

    action(['index', 'edit', 'show'], function () {
        ctrl.destroy = function (item) {
            Item.destroy({id: item.id}, function () {
                ctrl._reloadTable();
            })
        }
    });

    ctrl.search = function(){
        this._reloadTable();
    }

    ctrl._reloadTable = function() {
        if (ctrl.tableItems) {
            ctrl.tableItems.reload();
        }
    }
}]);
