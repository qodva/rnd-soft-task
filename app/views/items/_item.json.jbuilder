json.extract! item, :id, :caption, :description, :address, :tags, :category, :created_at, :updated_at
json.url item_url(item, format: :json)
