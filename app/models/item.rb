class Item < ApplicationRecord

  def self.search_fields
    ['caption', 'description']
  end

end
