class ItemsController < ApplicationController
  before_action :set_item, only: [:show, :edit, :update, :destroy]

  layout proc {
    if request.xhr?
      false
    else
      'application'
    end
  }

  # GET /items
  # GET /items.json
  def index
    filter_params = params.fetch(:filter, {})
    search_value = params.fetch(:search, '')

    # TODO: Replace to model class.
    dataset = Item.where([Item.search_fields.map {|f| "#{f} LIKE ?"}.join(' or ')] \
      + Array.new(Item.search_fields.length) { "%#{search_value}%"}
    )

    # TODO: Replace to model class.
    clauses = []
    filter_vals = []
    filter_params.each do |field, val|
      clauses.push("#{field} LIKE ?")
      filter_vals.push("%#{val}%")
    end

    dataset = dataset.where([clauses.join(' and ')] + filter_vals)

    respond_to do |format|
      format.html
      format.json {render json: [{count: dataset.count, data: dataset.page(params[:page]).per(params[:count]) }]}
    end
  end

  # GET /items/1
  # GET /items/1.json
  def show
    respond_to do |format|
      format.html
      format.json {render json: @item}
    end
  end

  # GET /items/new
  def new
    respond_to do |format|
      format.html
      format.json {render json: Item.new}
    end
  end

  # GET /items/1/edit
  def edit
    respond_to do |format|
      format.html
      format.json {render json: @item}
    end
  end

  # POST /items
  # POST /items.json
  def create
    @item = Item.new(item_params)

    if @item.save
      render json: {item: @item, msg: 'Item was successfully created.', redirect_to: 'items_path'}
    else
      render json: {errors: @post.errors, msg: @post.errors.full_messages.join(', ')}, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /items/1
  # PATCH/PUT /items/1.json
  def update
    if @item.update(item_params)
      render json: {item: @item, msg: 'Item was successfully updated.', redirect_to: 'items_path'}
    else
      render json: {errors: @post.errors, msg: @post.errors.full_messages.join(', ')}, status: :unprocessable_entity
    end
  end

  # DELETE /items/1
  # DELETE /items/1.json
  def destroy
    @item.destroy
    render json: { msg: 'Item was successfully deleted.', redirect_to: 'items_path' }
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_item
      @item = Item.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def item_params
      params.require(:item).permit(:caption, :description, :address, :tags, :category)
    end
end
