class CreateItems < ActiveRecord::Migration[5.0]
  def change
    create_table :items do |t|
      t.string :caption
      t.text :description
      t.text :address
      t.text :tags
      t.text :category

      t.timestamps
    end
  end
end
